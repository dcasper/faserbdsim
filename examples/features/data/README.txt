# Sample Data
Based on originalmodels/sc.gmad
2 files of 10 events with seeds 123 and 321
sample1.root - seed=123
sample2.root - seed=321

analysisConfig.txt was used with rebdsim to generate the analysis output files

ana1.root - analysis of sample1.root
ana2.root - anlaysis of sample2.root

combined-ana.root - from rebdsimCombine combined-ana.root ana1.root ana2.root

output.seedstate.txt - example ascii seed state information

fodo.root - generated from originalmodels/fodo.gmad with seed 456, 100 particles

optics.root - generated from fodo.root

=========================
In directory dataVersions
=========================
caches of previous data versions
sample1-vX.X.root
1 file with 10 events using originalModels/sc.gmad with seed 123

Examples & Tests
================

BDSIM includes a series of examples to illustrate its usage. These also form
the basis of the test suite to ensure stable development and record any changes.
Each example is self-contained and instructions are provided both in this
documentation and in the .rst files beside each example.


Basic Examples
--------------

.. toctree::
   :maxdepth: 3

   examples/airwatertarget
   examples/beamLoss
   examples/collimation-simple
   examples/simpleMachine
   examples/model-model.rst
   examples/target-simple.rst

.. _worked-examples:
   
Worked Examples
---------------

Detailed start-to-finish examples with explanation.

.. toctree::
   :maxdepth: 1

   worked_example_target/worked_example_target
   worked_example_collimation/worked_example_collimation
   worked_example_atf2


Specific Machines
-----------------

Converted examples of specific accelerators around the world.

.. toctree::
   :maxdepth: 3
   
   examples/atf2
   examples/diamond
   examples/lhc
   examples/ilc


Features
--------

These are tests which also act as examples of syntax.

.. toctree::
   :maxdepth: 3

   examples/features

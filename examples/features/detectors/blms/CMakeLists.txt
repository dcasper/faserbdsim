simple_testing(detector-blm-cube            "--file=blm-cube.gmad"      ${OVERLAP_CHECK})
simple_testing(detector-blm-cylindrical     "--file=blm-cylinder.gmad"  ${OVERLAP_CHECK})
simple_testing(detector-blm-sphere          "--file=blm-sphere.gmad"    ${OVERLAP_CHECK})
